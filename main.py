from input.camera import CameraImageProducer
from face_detector import process_faces
from output.rpc import RPCPeopleCountHandler
from processor.cv2 import CV2FaceCountDetector


def main():
    cip = CameraImageProducer(0)
    cip.open()
    rpc = RPCPeopleCountHandler('127.0.0.1', 8000, 'count')
    cv2processor = CV2FaceCountDetector()
    process_faces(image_producer=cip,
                  face_detector=cv2processor,
                  people_count_handler=rpc)
    cip.close()

if __name__ == '__main__':
    main()