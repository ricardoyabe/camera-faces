import cv2
import numpy as np

from processor import AbstractFaceCountDetector


class CV2FaceCountDetector(AbstractFaceCountDetector):
    def __init__(self):
        self.face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

    def count_faces(self, image: np.ndarray) -> int:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = self.face_cascade.detectMultiScale(gray, 1.1, 4)
        return len(faces)
