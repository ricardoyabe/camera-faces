import numpy as np
from abc import ABC, abstractmethod


class AbstractFaceCountDetector(ABC):
    @abstractmethod
    def count_faces(self, image: np.ndarray) -> int: ...
