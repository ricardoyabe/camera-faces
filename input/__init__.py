import numpy as np
from abc import ABC, abstractmethod


class AbstractImageProducer(ABC):
    @abstractmethod
    def get_image(self) -> np.ndarray: ...