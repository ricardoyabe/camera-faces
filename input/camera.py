import cv2
import numpy as np

from input import AbstractImageProducer


class CameraImageProducer(AbstractImageProducer):
    def __init__(self, camera_id: int):
        self.camera_id = camera_id

    def open(self):
        self.cap = cv2.VideoCapture(self.camera_id)
        if not self.cap.isOpened():
            raise RuntimeError('Unable to open camera: {}'.format(self.camera_id))

    def close(self) -> None:
        self.cap.release()

    def get_image(self) -> np.ndarray:
        if self.cap.isOpened():
            ret, frame = self.cap.read()
            if ret:
                return frame
            else:
                raise RuntimeError('Invalid ret: {}'.format(ret))
        else:
            raise RuntimeError('Camera is not opened')
