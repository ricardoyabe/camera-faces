from input import AbstractImageProducer
from output import AbstractPeopleCountHandler
from processor import AbstractFaceCountDetector


def process_faces(image_producer: AbstractImageProducer,
                  face_detector: AbstractFaceCountDetector,
                  people_count_handler: AbstractPeopleCountHandler):
    for _ in range(10):
        img = image_producer.get_image()
        number_of_faces = face_detector.count_faces(img)
        people_count_handler.process_people_count(number_of_faces)
