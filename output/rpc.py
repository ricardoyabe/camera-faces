import requests

from output import AbstractPeopleCountHandler


class RPCPeopleCountHandler(AbstractPeopleCountHandler):
    def __init__(self, url: str, port: int, path: str):
        self.url = 'http://{}:{}/{}'.format(url, port, path)

    def process_people_count(self, number_of_faces: int) -> None:
        requests.post(self.url, data={'count': number_of_faces})