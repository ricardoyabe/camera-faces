from abc import ABC, abstractmethod


class AbstractPeopleCountHandler(ABC):
    @abstractmethod
    def process_people_count(self, number_of_people: int) -> None: ...